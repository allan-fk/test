<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/user', 'UserController@index');

Route::resource('shares', 'ShareController');

Route::get('/yo/{name}', 'ShareController@index');

Route::get('/test', 'TestController@showIndex');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('profile', function () {
    // Only verified users may enter...
})->middleware('verified');