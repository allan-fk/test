<?php

$dsn = 'mysql:dbname=people;host=127.0.0.1';

try {
    $dbh = new PDO($dsn, "root");
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$listPeople = function() use ($dbh) {

    $stmt = $dbh->query('SELECT * FROM `people` LIMIT 10000', PDO::FETCH_ASSOC);

    return $stmt->fetchAll();
};

$countPeopleByDiet = function () use ($dbh) {
    $sql = 'SELECT * '
            . 'FROM people '
            . 'INNER JOIN category ON people.category_id = category.id '
            . 'WHERE category.name IN("' . str_replace(',', '","', $_GET['str']) . '")';
    
    $stmt = $dbh->query($sql, PDO::FETCH_ASSOC);
    $count = count($stmt->fetchAll());

    return ['count' => $count];
};

$countPeopleFrom = function () use ($dbh) {

    $stmt = $dbh->query('SELECT * FROM `people` WHERE email LIKE "%@' . $_GET['mail_domain'] . '"', PDO::FETCH_ASSOC);
    $count = count($stmt->fetchAll());

    return ['count' => $count];
};

$resources = [
    'people' => $listPeople,
    'countPeopleFrom' => $countPeopleFrom,
    'countPeopleByDiet' => $countPeopleByDiet
];

echo json_encode($resources[$_GET['resource']]());