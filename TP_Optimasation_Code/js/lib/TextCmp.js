
(function () {
    
    const Component = module.import('Component')

    class NumberCmp extends Component {
        render () {
            let root = document.querySelector(this.querySelector);

            root.innerHTML = this.props.value;
        }
    }

    module.exports('TextCmp', NumberCmp)

})()