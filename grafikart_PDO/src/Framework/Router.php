<?php

namespace Framework;

use Framework\Router\Route;
use Framework\Router\MiddlewareApp;
use Zend\Expressive\Router\RouteResult;
use Psr\Http\Server\MiddlewareInterface;
use Zend\Expressive\Router\FastRouteRouter;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\Route as ZendRoute;
//use Zend\Expressive\Router\Middleware\RouteMiddleware;

/**
 * Register and match routes
 */

class Router
{
    /**
     * @var type => FastRouteRouter
     */
    
    private $router;

    public function __construct()
    {
        $this->router = new FastRouteRouter();
    }

    /**
     * @param string $path
     * @param callable $callable => La function a appeler
     * @param string $name
     */

    public function get(string $path, callable $callable, string $name = null)
    {
        $this->router->addRoute(new ZendRoute($path, new MiddlewareApp($callable), ['GET'],$name));
    }

    /**
     * @param serverRequestInterface $request
     * @return Route
     */

    public function match(ServerRequestInterface $request): ?Route
    {
        $result = $this->router->match($request);
        // $request = $request->withAttribute(RouteResult::class, $result);
        if ($result->isSuccess()) {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedRoute()->getMiddleware()->getCallback(),
                $result->getMatchedParams()
            );
        }
        return null;
    }

    public function generateUri(string $name, array $params): ?string
    {
        return $this->router->generateUri($name, $params);
    }
}
