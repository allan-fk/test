<?php

namespace Tests\Framework;

use Framework\App;
use App\Blog\BlogModule;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Tests\Framework\Modules\StringModule;
use Tests\Framework\Modules\ErroredModule;
// use Test\Framework\Modules\ErroredModule;

class AppTest extends TestCase 
{
  public function testRedirectTrailingSlashs()
  {
    $app = new App();
    $request = new ServerRequest('GET', "/demoslash/");
    $response = $app->run($request);
    $this->assertContains("/demoslash", $response->getHeader("Location"));
    $this->assertEquals(301, $response->getStatusCode());
  }

  public function testBlog()
  {
    $app = new App([
      BlogModule::class
    ]);
    $request = new ServerRequest('GET', "/blog");
    $response = $app->run($request);
    $this->assertContains("<h1>Bienvenue sur le blog</h1>", (string)$response->getBody());
    $this->assertEquals(200, $response->getStatusCode());

    $requestSingle = new ServerRequest('GET', "/blog/mon-article");
    $responseSingle = $app->run($requestSingle);
    $this->assertContains("<h1>Bienvenue sur l'articlemon-article</h1>", (string)$responseSingle->getBody());
  }

  public function testThrowExceptionIfNoReponseSent()
  {
    $app = new App([
      ErroredModule::class
    ]);
    $request = new ServerRequest('GET', "/demo");
    $this->expectException(\Exception::class);
    $app->run($request);
  }

  public function testConvertStringToResponse()
  {
    $app = new App([
      StringModule::class
    ]);
    $request = new ServerRequest('GET', "/demo");
    $response = $app->run($request);
    $this->assertInstanceOf(ResponseInterface::class, $response);
    $this->assertEquals('DEMO', (string)$response->getBody());
  }

  public function testError404()
  {
    $app = new App();
    $request = new ServerRequest('GET', "/aze");
    $response = $app->run($request);
    $this->assertContains("<h1>Erreur 404</h1>", (string)$response->getBody());
    $this->assertEquals(404, $response->getStatusCode());
  }
}