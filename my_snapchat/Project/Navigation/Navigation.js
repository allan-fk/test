// Navigation/Navigation.js
import {createStackNavigator} from 'react-navigation'
import Home from '../Components/Home'
import SignUp from '../Components/SignUp'

const SearchStackNavigator = createStackNavigator({
  Home: { // Ici j'ai appelé la vue "Search" mais on peut mettre ce que l'on veut. C'est le nom qu'on utilisera pour appeler cette vue
    screen: Home,
    navigationOptions: {
      title: "Page d'accueil"
    }
  },
  SignUp: { 
    screen: SignUp
  }
})

export default SearchStackNavigator