/**
 * @Author: klein
 * @Date:   2018-06-07T02:11:54+02:00
 * @Email:  https://allan-fk.gitlab.io
 * @Filename: ex_06.js
 * @Last modified by:   klein
 * @Last modified time: 2018-06-17T23:27:52+02:00
 * @License: GNU General Public License
 */



var description;
window.onload = function() {
  var txtBox = document.getElementsByTagName('div')[2];
  var codeSample = document.getElementsByTagName('pre')[0].innerHTML;
  var jsFile = "/home/klein/Documents/works/Php_works/Racing_JS/script/test.txt";
  //var jsFile = new Test(include);
  var file = IO.getFile(jsFile);
  var stream = IO.newOutputStream(file, "text");
  stream.writeString("This is some text");
  stream.close();
  txtBox.innerHTML = codeSample;
};
