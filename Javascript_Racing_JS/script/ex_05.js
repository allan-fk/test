window.onload = function() {
  var body = document.body;
  var style = window.getComputedStyle(document.body).getPropertyValue('font-size');
  var more = document.getElementsByTagName('button')[0];
  var less = document.getElementsByTagName('button')[1];
  var txt = document.getElementsByTagName('section')[0];
  var menu =  document.getElementsByTagName('select')[0];
  var currentFontSize = parseFloat(style);

// Change size
  more.onclick = function() {
    currentFontSize++;
    body.style.fontSize = currentFontSize +'px';
    console.log(currentFontSize);
  }
  less.onclick = function() {
    currentFontSize--;
    body.style.fontSize = currentFontSize +'px';
    console.log(currentFontSize);
  }

// Change background
  menu.onchange = function() {
    var x = menu.value;
    document.body.style.backgroundColor = x;
  }
};
