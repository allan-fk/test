/**
 * @Author: klein
 * @Date:   2018-06-05T21:17:57+02:00
 * @Email:  https://allan-fk.gitlab.io
 * @Filename: ex_03.js
 * @Last modified by:   klein
 * @Last modified time: 2018-06-17T23:26:12+02:00
 * @License: GNU General Public License
 */



window.onload = function() {
  var content = document.body.children[0];
  var child = content.children[0];
  var div = child.children[2];
  var select = div.children[0];
  select.onclick = function() {
    var txt;
    var person = "";
    loop1:
      while (person == null || person == "") {
        person = prompt("Quel est votre nom ?");
        if (person !== null && person !== "") {
          if (confirm('Etes vous sûr que \"' + person + '\" est votre nom ?')) {
            select.innerText = 'Bonjour \"' + person + '\" !';
            alert('Bonjour \"' + person + '\" !');
          } else {
            person = "";
            continue loop1;
          }
        }
      }
  }
};
