/**
 * @Author: klein
 * @Date:   2018-06-05T16:32:02+02:00
 * @Email:  https://allan-fk.gitlab.io
 * @Filename: ex_01.js
 * @Last modified by:   klein
 * @Last modified time: 2018-06-17T23:20:12+02:00
 * @License: GNU General Public License
 */



window.onload = function() { // Au chargement de la fenetre
		var content = document.body.children[0];
		var child = content.children[0];
		var div = child.children[2];
		var txt = div.children[0];
		txt.innerText = "Hello World";
};
