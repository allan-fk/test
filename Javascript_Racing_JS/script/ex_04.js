/**
 * @Author: klein
 * @Date:   2018-06-06T13:30:30+02:00
 * @Email:  https://allan-fk.gitlab.io
 * @Filename: ex_04.js
 * @Last modified by:   klein
 * @Last modified time: 2018-06-17T23:26:34+02:00
 * @License: GNU General Public License
 */



window.onload = function() { // Au chargement de la fenetre
    var txt = document.getElementsByTagName('p')[0].innerHTML;
		var txtBox =  document.getElementsByTagName('div')[2];
		txtBox.innerHTML = txt.slice(11,-55);
		console.log(txt.slice(0,-55));
};
