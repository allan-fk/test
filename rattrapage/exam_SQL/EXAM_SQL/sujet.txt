==============================================================================
			Details administratifs
==============================================================================

-	Aucune forme de communication n'est permise. Il est interdit de
	discuter, d'ecouter de la musique, de faire du bruit, de perturber ou
	de nuire, de quelque maniere que ce soit, le bon deroulement de
	l'examen.

-	Vous pouvez vous munir de feuilles vierges, d'un stylo et d'une
	bouteille d'eau. Tout autre objet, materiel ou nourriture est
	strictement interdit.

-	Vos telephones portables doivent etre eteints et ranges dans vos sacs.
	Ces derniers doivent etre deposes a l'endroit indique par le ou les
	surveillants.

-	Il faudra utiliser le depot de rendu "EXAM_SQL_03", qui contiendra
	les fichiers aux noms demandes dans les exercices.

-	Les droits de ramassage doivent être donnés à
	guillaume1.lefrant@epitech.eu ET ramassage-tek

-	C'est un programme qui s'occupe du ramassage, respectez les noms, les
	chemins, les fichiers et les repertoires.

-	Toute sortie est definitive.

-	Toute fraude (ou tentative) sera sanctionne par la note de -42.

-	Vous devez creer un fichier "auteur" qui doit
	contenir votre login, suivi d'un retour a la ligne.

		user@exam> cat -e auteur
		prenom.nom@epitech.eu$
		user@exam>

==============================================================================
				Preliminaires
==============================================================================

-	Toutes les questions sont independantes. Vous etes libres de les
	traiter dans l'ordre que vous voulez. Une bonne reponse vaut 1 point,
	une mauvaise reponse vaut 0 point.

-	Vous avez le droit d'utiliser qu'une seule fois chacun des mots cles
	"SELECT", "AND" et "OR" par question. Toute reponse comportant plus
	d'une fois l'un de ces mots cles sera consideree comme fausse.

-	Pour chaque question, vous devez rendre un fichier nomme "ex_XX.sql"
	ou "XX" est le numero de la question sur 2 chiffres. Par exemple, pour
	l'exercice 05 le fichier doit se nommer "ex_05.sql".

-	Un fichier cinema.sql est disponible pour initialiser la base si
	nécessaire.

==============================================================================
				Questions
==============================================================================

01
	Faire une requete qui affiche le numero des salles et leur nom, pour
	celles qui comportent plus de 200 sieges et qui ne sont pas au
	rez-de-chaussee. Les colonnes seront nommees "Numero des salles" et
	"Nom des salles".

--
02
	Faire une requete qui affiche la description de la table des films.

--
03
	Faire une requete qui affiche l'identifiant pour les films dont	le
	titre comporte la chaine de caracteres "tard" (insensible a la casse).
	La colonne sera nommee "Identifiant".

--
04
	Faire une requete qui affiche le nom du genre et le nombre de films
	pour chaque genre. Trie par nombre de films decroissant puis par nom
	de genre croissant. Les colonnes seront nommees "Nom du genre" et
	"Nombre de films".

--
05
	Faire une requete qui affiche la liste des tables de la base de
	donnees.

--
06
	Faire une requete qui affiche pour chaque etage son numero, le nombre
	total de sieges et le nombre total de salles, trie par nombre de
	sieges croissant. Les colonnes seront nommees "Numero etage", "Nombre
	total de sieges" et "Nombre total de salles".

--
07
	Faire une requete qui affiche toutes les reductions du cinema, tries
	par pourcentage de reduction croissant puis par nom decroissant.
	Toutes les colonnes seront affichees et dans le meme ordre qu'en base
	de donnees.

--
08
	Faire une requete qui affiche la date courante au format "YYYY-MM-DD".
	La colonne sera nommee "Aujourd'hui".


--
09
	Faire une requete qui affiche le nombre de films qui n'ont jamais ete
	vus par les membres. La colonne sera nommee "Nombre de films jamais
	vus".

--
10
	Faire une requete qui affiche le nombre total de films du genre
	"western" dont le distributeur est "tartan films" ou "lionsgate uk",
	ou que le titre commence par un 'T'. La colonne sera nommee "Nombre de
	films 'western'".

--
11
	Faire une requete qui affiche le titre des 12 premiers films. La
	colonne sera nommee "Titre des 12 premiers films".

--
12
	Faire une requete qui affiche toutes les reductions du cinema, tries
	par pourcentage de reduction croissant puis par nom decroissant.
	Toutes les colonnes seront affichees et dans le meme ordre qu'en base
	de donnees.

--
13
	Faire une requete qui affiche les 92 premiers caracteres du resume
	pour les films dont l'id est impair et compris entre 42 et 84 (exclus).
	Si jamais le resume fait plus de 92 caracteres, alors on affichera
	les 89 premiers caracteres suivi de "..." colle.
	La colonne sera nommee "resum ...".

--
14
	Faire une requete qui affiche le nombre total de salles et multiplier
	ce nombre par 3. La colonne sera nommee "3 fois le nombre total de
	salles".

--
15
	Faire une requete qui affiche le nombre de membres et leur age moyen.
	Les colonnes seront nommees "Nombre de membres" et "Age moyen" (il est
	attendu un nombre strictement positif, arrondi a l'entier le plus
	proche). Attention, pour definir l'age on ne prendra que l'annee.

--
16
	Faire une requete qui affiche seulement les codes postaux pour
	lesquels on a plusieurs personnes qui les possedent et afficher la
	liste des codes postaux dans l'ordre croissant.
	La colonne sera nommee "Codes postaux".

--
17
	Faire une requete qui affiche le titre du film et le nom du
	distributeur pour les films dont l'identifiant est 21, 87, 263, 413 ou
	633. Les colonnes seront nommees "Titre du film" et "Nom du
	distributeur".

--
18
	Faire une requete qui affiche le nom de l'abonnement le plus cher,
	ainsi que son prix. Les colonnes seront nommees "Nom de l'abonnement
	le plus cher" et "Prix".

--
19
	Faire une requete qui affiche la duree en minutes, du film le plus
	court. Les films ayant NULL ou 0 de renseigne en duree ne doivent pas
	etre pris en compte. La colonne sera nommee "Duree du film le plus
	court".

--
20
	Faire une requete qui affiche le nombre de films produits par annee,
	trie par annee de production decroissante. Les colonnes seront nommees
	"Nombre de films" et "Annee de production".

--
