<?php
function huffman($text){
  // Création d'un tableau d'occurences
  $tree = array();
  foreach (count_chars($text, 1) as $key => $value) {
    echo "Il y a $value occurence(s) de \"" , chr($key) , "\" dans la phrase.\n";
    $tree[] = array('nbr' => $value, 'letter' => chr($key));
    //$i++;
  }
  $nbr  = array_column($tree, 'nbr');
  $letter = array_column($tree, 'letter');
  array_multisort($nbr, SORT_DESC, $letter, SORT_ASC, $tree);

  // Création des bits selon le nombre d'occurences
  $y = 0;
  $bits = 1;
  while ($bits < count($tree)) {
    $max = (1 << $bits);
      for ($i = 0; $i < $max && $y < count($tree); $i++) {
          $tree[$y]['bin'] = str_pad(decbin($i), $bits, '0', STR_PAD_LEFT);
          echo "\n";
          $y++;
      }
      $bits ++;
  }
  $fp = fopen('yo', 'w');
  $bin = '';
  // le contenu de 'data.txt' est maintenant 123 et non 23 !
  $strToBinary = str_split($text);
  foreach($strToBinary as $char){
    $i = 0;
    while ($char != $tree[$i]['letter']) {
        $i++;
    }
    //echo $char;
    $bin .= $tree[$i]['bin'];
    
  }
  $write = pack('H*', base_convert($bin, 2, 16));
  fwrite($fp, $write);
  //echo $bin;
  fclose($fp);
  $value = unpack('H*', $bin);
  echo base_convert($value[1], 16, 2);
  var_dump($tree);
  var_dump(count($tree));

}

$txt2 = "cagataagagaa";
$txt = "Je me presente, je m'appelle Henry
Je voudrais bien réussir ma vie, être aime
Être beau, gagne de l'argent
Puis surtout être intelligent
Mais pour tout ça il faudrait que je bosse a plein temps
Je suis chanteur, je chante pour les copains
Je veux faire des tubes et que ça tourne bien, tourne bien
Je veux ecrire un chanson dans le vent
Un air gai, chic et entrainant
Pour faire danser dans les soirées de M. Durand";
huffman($txt2);