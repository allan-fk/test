<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <title>Compresseur Huffman</title>
   </head>
   <body>
      <h1>Compresseur Huffman</h1>
<?php
    require 'entropie-include.php';

# décommenter pour passer en mode test (cf exercice 2)
    $test_mode = 1;

    $texte = "";
    if (isset($_POST["texte"])) {
       $texte = $_POST["texte"];
       list ($occurences, $total) = occurences($texte);

       if (isset($test_mode)) {
    # pour tester avec l'exercice du cours!
    //$texte = "correct";
    //$total = 64;
    /*$occurences = array("e" => 15,
                              "r" => 12,
                              "c" => 11,
                              "o" => 10,
                              "t" => 9,
                              "l" => 4,
                              "m" => 2,
                              "k" => 1);*/
          echo "<p>Mode de test!</p>";
       }
       ?>
       <p>La chaîne fait <?php echo $total; ?> symbole(s) dont
          <?php 
              $listOcTxt = array_count_values(str_split($texte));
              $listOc = [];
              foreach ($listOcTxt as $key => $value) {
                if ($value == 1){
                  $listOc[] += $value;
                }
              }
              echo count($listOc);
          ?> unique(s).
          Les symboles se répartissent comme suit:
          <ul>
          <?php dump($occurences); ?>
           </ul></p>
       <?php
       if ($total) {
    $entropie = calculer_entropie($occurences, $total);
    ?>
    <p>L'entropie calculée vaut: <?php echo $entropie; ?></p>
       <?php
       }

       $code = huffman($occurences);
       ?>
       <p>Voici la table de codage Huffman:
         <ul>
       <?php dump($code); ?>
       </ul></p>
       <p>Et voici votre texte compressé (sous forme de 0 et de 1 textuels):
       <?php echo compress($texte, $code); ?>
       </p>
       <?php
    }
?>
      <h2>Texte à compresser</h2>
      <form action="" method="post">
      <p><textarea name="texte" rows="10" cols="60"><?php echo htmlentities($texte, ENT_XHTML, "ISO-8859-1"); ?></textarea>
         <input type="submit" />
      </p>
      </form>
   </body>
</html>
<?php

/**
 * @param string à échapper
 * @return string échappée
 */
function html_protect($s) {
   return (ctype_graph($s)
           ? htmlentities($s, ENT_XHTML, "ISO-8859-1")
           : "ASC " . join(", ", array_map("ord", str_split($s))));
}

/**
 * @param array[string]string $a tableau associatif à afficher, suppose
 *                               que $value est déjà échappé
 */
function dump ($a) {
   foreach ($a as $key => $value) {
      echo "<li>" . html_protect($key)  . ": " . $value . "</li>";
   }
}

/**
 * @param array[string]integer tableau associatif symbole => occurence
 * @return array[string]string tableau associatif symbole => code
 */
function huffman($o) {
   global $test_mode;

   $code = array();

   # à compléter ...

   return $code;
}

/**
 * @param string $t texte dont les symboles sont à compresser
 * @param array[string]string tableau associatif symbole => code
 * @return string chaîne de bits en format texte
 */
function compress($t, $c) {
   $s = "";

   # à compléter

   return $s;
}

?>