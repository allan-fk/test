<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Mycinema</title>
  <!-- CSS -->
  <link rel="stylesheet" href="css/styles.css">
  <!-- FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
  <!-- bootstrap css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css">
</head>

<body>
  <div class="wallpaper"></div>
  <button type="button" class="btn btn-primary admin">Admin</button>
  <!-- INIT DB -->
  <?php
  require 'db/c.php';
  $db=Database::connect();
  include 'db/controller.php';
  ?>
    <div class="container">
      <div class="row">
        <!-- SEARCH BAR -->
        <h1 class="super_title"><img src="img/flag-algeria.png" alt="">92iLa~PIr@teriz--xxXULTIMATE STREAMXxx--92iLa~PIr@teriz<img src="img/flag-algeria.png" alt=""></h2>
      <div id="custom-search-input">
        <div class="input-group col-md-12">
          <form method="GET" id="auto-suggest">
          <input type="search" name="mot" class="search search-query form-control" placeholder="Search" value="Search Movie or :m = membre / :d = distributeur" onfocus="if(this.value=='Rechercher...')this.value=''" onblur="if(this.value=='')this.value='Rechercher...'" autocomplete="off"/>
          <span class="input-group-btn">
              <button class="btn btn-danger" type="input">
                <span class=" glyphicon glyphicon-search"></span>
          </button>
          </span>
          </form>
        </div>
      </div>
      <!--HOME-->
      <? if (empty($_GET)) { ?>
        <?php include 'view/search/home.php'; ?>
      <? } ?>
      <!-- TABLEAU -->
      <?php if (!empty($_GET)) { ?>
        <!--- DISTRIBUTION--->
        <?php if (isset($distribs) && $nbr_total_articles > 0) { button(); ?>
          <?php include 'view/search/distribution.php'; ?>
        <!--- MENBRES--->
        <?php } else if (isset($membres) && $nbr_total_articles > 0) { button(); ?>
          <?php include 'view/search/membres.php'; ?>
        <!--- FILMS --->
        <?php } else if (isset($films) && $nbr_total_articles > 0) { button(); ?>
          <?php include 'view/search/movies.php'; ?>
        <?php } else {
          echo '<p class="sorry">Désolé mais aucun résultat trouvé pour' . $mot . '</p>';
        }
      } ?>
    </div>
  </div>
</div>
  <script type="text/javascript" src="js/script.js"></script>
</body>
</html>
