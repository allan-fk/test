<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mycinema</title>
  <!-- CSS -->
  <link rel="stylesheet" href="css/profil.css">
  <!-- FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <!-- bootstrap css -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>

<body>

  <!-- INIT DB -->
  <?php
  require 'db/c.php';
  $db=Database::connect();
  include 'db/controller_member.php';
  ?>
  <div class="wallpaper"></div>
    <div class="container center">
      <div class="row profile">
        <div class="col-lg-3">
          <div class="profile-sidebar">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
              <img src="img/LoginIcon.png" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
                <?php
                $membre = $perso->fetch();
                echo strtoupper($membre['nom'] . ' ' . $membre['prenom']);
                ?>
              </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->
            <!-- SIDEBAR BUTTONS -->
            <div class="profile-userbuttons">
              <button type="button" class="btn btn-success btn-sm">
                <?php
                $info_abo = $abos->fetch();
                echo strtoupper($info_abo['nom']) . ' ';
                if ($info_abo['nom'] == "VIP"){
                  echo '<img src="img/434779.svg" alt="">';
                } else if ($info_abo['nom'] == "GOLD") {
                  echo '<img src="img/434780.svg" alt="">';
                } else if ($info_abo['nom'] == "Classic") {
                  echo '<img src="img/434809.svg" alt="">';
                } else if ($info_abo['nom'] == "pass day") {
                  echo '<img src="img/434768.svg" alt="">';
                } else if ($info_abo['nom'] == "malsch") {
                  echo '<img src="img/434765.svg" alt="">';
                }
                ?>
              </button>
            </div>
            <!-- END SIDEBAR BUTTONS -->
            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
              <ul class="nav">
                <li>
                  <a href="#">
  							<i class="glyphicon glyphicon-plus"></i>
  							Ajouter un film </a>
                </li>
                <li>
                  <a href="#">
  							<i class="glyphicon glyphicon-film"></i>
  							Liste complete des films </a>
                </li>
                <li>
                  <? echo "<a href=\"javascript:history.go(-1)\">";?>
  							<i class="glyphicon glyphicon-arrow-left"></i>
  							Retour</a>
                </li>
              </ul>
            </div>
            <!-- END MENU -->
          </div>
        </div>
        <div class="col-lg-9">
          <div class="profile-content">
            <p class="list_title">DERNIERS FILMS :</p></br>
            <?
            //include('db/IMDbapi.php');


            //while($film = $historique->fetch())
            $i=0;
            foreach($historique as $film)
            {
              if(++$i > 6) break;
              $string = str_replace(' ', '+', $film['titre']);
              $test = file_get_contents("https://www.themoviedb.org/search/movie?query=" . $string);
              $mystring = 'abc';
              $findme   = 'data-src="https://image.tmdb.org/t/p';
              $limit = '.jpg';
              $pos = strpos($test, $findme);
              $word = substr( $test , $pos, 100 );
              $url = strpos($word, $limit);
              $cut = substr( $word , 10 , $url-6 );
              echo '<div class="col-md-4 cover">';
              if (substr($cut, -3) !== "jpg") {
                echo   '<img src="img/not_found.jpg" alt="">';
              } else {
                echo   '<img src="' .  $cut . '" alt="">';
              }
              echo '<p class="movie_title one">' . $film['titre'] . '</p><br>';
              echo '<p class="movie_title two">' . substr($film['date'], 0, -9) . '</p>';
              echo '<div class="rating">
<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
</div>';
              echo '</div>';
            }
            /*
            $imdb = new IMDbapi('LICe5WdtK0mwEMcgJGpSmgm0kDfxmz');
            $data = $imdb->title($film['titre']);
            $ARRAY = json_decode($data,true);
              echo '<div class="col-md-4 cover">';
              echo   '<img src="' .   $ARRAY["poster"] . '" alt="">';
              echo $film['titre'];
              echo '</div>';
            }*/
            ?>
            <!--
            <div class="col-md-4 cover">
              <img src="https://m.media-amazon.com/images/M/MV5BMTEzNzY0OTg0NTdeQTJeQWpwZ15BbWU4MDU3OTg3MjUz._V1_SY1000_CR0,0,674,1000_AL_.jpg" alt="">
            </div>
            <div class="col-md-4 cover">
              <img src="https://m.media-amazon.com/images/M/MV5BMTEzNzY0OTg0NTdeQTJeQWpwZ15BbWU4MDU3OTg3MjUz._V1_SY1000_CR0,0,674,1000_AL_.jpg" alt="">
            </div>
            <div class="col-md-4 cover">
              <img src="https://m.media-amazon.com/images/M/MV5BMTEzNzY0OTg0NTdeQTJeQWpwZ15BbWU4MDU3OTg3MjUz._V1_SY1000_CR0,0,674,1000_AL_.jpg" alt="">
            </div>
            <div class="col-md-4 cover">
              <img src="https://m.media-amazon.com/images/M/MV5BMTEzNzY0OTg0NTdeQTJeQWpwZ15BbWU4MDU3OTg3MjUz._V1_SY1000_CR0,0,674,1000_AL_.jpg" alt="">
            </div>
            <div class="col-md-4 cover">
              <img src="https://m.media-amazon.com/images/M/MV5BMTEzNzY0OTg0NTdeQTJeQWpwZ15BbWU4MDU3OTg3MjUz._V1_SY1000_CR0,0,674,1000_AL_.jpg" alt="">
            </div>-->
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
    <?php Database::disconnect();?>

  </body>
  </html>
