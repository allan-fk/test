@foreach($annonces as $annonce)
<div class="row product-list dev">
    <div class="col-md-9 col-sm-6 product-item animation-element slide-top-left">
        <div class="product-container">
            <div class="row">
                <div class="col-md-3"><a href="#" class="product-image"><img src='storage/{{$annonce['picture_one']}}'></a></div>
                <div class="col-xs-8">
                    <h2><a href="#">{{$annonce['name']}}</a></h2>
                </div>
                <div class="col-md-1"><i class="far fa-heart" style="font-size: 20px;"></i></div>
                <div class="col-md-9 col-xs-12">
                    <p class="product-description" style="font-size: 13px;">{{$annonce->category->name}}</p>
                    <p class="product-description" style="font-size: 13px;margin: 10px 0px;margin-top: 0px;margin-bottom: 20px;">Paris
                        75017</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-warning" style="font-weight: bold;">50€</p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-right" style="font-size: 12px;font-weight: bold;">23 octobre 2018</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach