<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>annonces_create</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/lumen/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/user.css">
</head>
<body>
    <nav class="navbar navbar-default" style="background-color: #F56B2A;">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="#" style="color: #ffffff;font-weight: bold;">Leboncoin</a><button class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-left">
                    <li class="active" role="presentation"><a href="#" style="color: #ffffff;">déposer une annonce</a></li>
                    <li role="presentation"><a href="#" style="color: #ffffff;">offres</a></li>
                    <li role="presentation"><a href="#" style="color: #ffffff;">demandes</a></li>
                    <li class="active" role="presentation"><a href="#" style="color: #ffffff;">mes favoris</a></li>
                    <li role="presentation"><a href="#" style="color: #ffffff;">boutiques</a></li>
                    <li role="presentation"><a href="#" style="color: #ffffff;">messages</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active" role="presentation"><a class="text-uppercase" href="#" style="color: #ffffff;"><i class="fas fa-user" style="margin: 0px 5px;"></i>Epicure</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="jumbotron hero" style="background-image: url(&quot;unset&quot;);background-color: #FAFAFA;margin: 0px;">
        <div class="container">
            <form action="{{ action('AnnonceController@store') }}" enctype="multipart/form-data" method="post" id="contactForm">
                @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div id="successfail"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="message">
                            <fieldset>
                                <legend><i class="icon ion-compose" style="margin: 10px;"></i>&nbsp;Votre annonce&nbsp;</legend>
                            </fieldset>
                            <div class="form-group has-feedback" style="width: 50%;"><label class="control-label" for="from_name">Catégorie</label>{!! Form::select('category', $categories, null, ['class' => 'form-control']) !!}</div>
                            <div class="form-group has-feedback" style="width: 50%;"><span style="width: 30%;"><label class="control-label checkbox-inline" style="margin: 0px 10px;"><input type="checkbox">Offre</label></span>
                                <span
                                    style="width: 32%;"><label class="control-label checkbox-inline"><input type="checkbox">Demande</label></span>
                            </div>
                            <div class="form-group has-feedback" style="width: 50%;"><label class="control-label" for="from_email">Titre de l'annonce</label><input class="form-control"
                                    type="text" name="name"></div>
                            <div class="form-group has-feedback" style="width: 50%;"><label class="control-label" for="from_email">Texte de l'annonce</label><textarea class="form-control"
                                    rows="8" name="description"></textarea></div>
                            <div class="form-group has-feedback" style="width: 30%;"><label class="control-label" for="from_email">Prix</label><input class="form-control" type="number" name="price"></div>
                            <div class="form-group has-feedback" style="width: 60%;"><label class="control-label" for="from_email">Photos :&nbsp;Une annonce avec photo est 7 fois plus consultée qu'une annonce sans photo</label>
                                {!! Form::file('image'); !!}
                                <input type="file" accept="image/*" style="margin: 20px;">
                                <input type="file" accept="image/*"
                                        style="margin: 20px;"></div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend> <i class="fa fa-location-arrow" style="margin: 10px;"></i>Localisation</legend>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label class="control-label">Ville ou code postal</label><input class="form-control"
                                                    type="text"></div>
                                            <div class="form-group"><label class="control-label">Adresse&nbsp;</label><input class="form-control"
                                                    type="text"></div>
                                            <div class="alert alert-info" role="alert"><span><br>		Complétez votre adresse et les personnes utilisant la recherche autour de soi trouveront plus facilement votre annonce.<br>		Si vous ne souhaitez pas renseigner votre adresse exacte, indiquez votre rue sans donner le numéro. <br>		Cette information ne sera conservée que le temps de la publication de votre annonce.<br></span></div>
                                            <hr class="visible-xs-block">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="static-map"><a href="https://www.google.com/maps/place/2+15th+St+NW+Washington+DC+20024"
                                                    target="_blank"><img class="img-responsive" src="https://img.phonandroid.com/2015/06/googlemaps_paris.jpg" alt="Google Map of Washington Monument" width="500px"></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend><i class="far fa-user" style="margin: 10px;"></i>Vos informations</legend>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label class="control-label">Email</label><input class="form-control" type="email"></div>
                                            <div class="form-group"><label class="control-label">Téléphone</label><input class="form-control" type="tel"></div>
                                            <div class="checkbox"><label class="control-label"><input type="checkbox">Masquer le numéro de téléphone dans l'annonce.&nbsp;&nbsp;</label></div>
                                            <hr class="visible-xs-block">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-right" style="font-size: 13px;font-weight: bold;"><strong>Champs obligatoires *</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div><button class="btn btn-primary btn-lg" type="submit" style="margin: 0px 20px;">Valider</button>
                            <hr class="visible-xs-block visible-sm-block">
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <section class="testimonials" style="background-color: #F2F2F2;margin: 0px;">
        <div class="row" style="margin: 0px;">
            <div class="col-md-12">
                <div class="container">
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group&nbsp;</span>
                                <ul>
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="site-footer" style="background-color: #474747;color: rgb(255,255,255);padding: 20px;">
        <div class="row" style="margin: 0px;">
            <div class="col-md-12">
                <div class="container">
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group Item 1</span>
                                <ul class="list-unstyled">
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group Item 1</span>
                                <ul class="list-unstyled">
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group Item 1</span>
                                <ul class="list-unstyled">
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-group">
                            <li class="list-group-item" style="background-color: transparent;"><span>List Group Item 1</span>
                                <ul class="list-unstyled">
                                    <li>Item 1</li>
                                    <li>Item 2</li>
                                    <li>Item 3</li>
                                    <li>Item 4</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <p>Partenaires :<a href="http://www.younited-credit.com">Younited Credit</a><a href="https://emploicadres.leboncoin.fr/?utm_source=leboncoin&amp;utm_medium=footer&amp;utm_campaign=permanent">leboncoin Emploi Cadres</a>
                            <a
                                href="http://www.agriaffaires.com/?utm_source=partner_lbc">Agriaffaires</a>
                                <a href="http://www.machineryzone.fr/?utm_source=partner_lbc">MachineryZone</a><a href="http://www.ledenicheur.fr">leDénicheur</a>
                                <a
                                    href="https://www.avendrealouer.fr">AVendreALouer</a><a href="https://immobilierneuf.leboncoin.fr">leboncoin Immobilier Neuf</a></p>
                    </div>
                    <div class="col-md-3">
                        <p class="text-left">Paragraph</p>
                    </div>
                    <div class="col-md-3 col-md-push-6">
                        <p class="text-right">Paragraph</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/Animated-Pretty-Product-List-v12.js"></script>
    <script src="assets/js/Contact-FormModal-Contact-Form-with-Google-Map.js"></script>
    <script src="assets/js/Grid-and-List-view-V10.js"></script>
</body>

</html>