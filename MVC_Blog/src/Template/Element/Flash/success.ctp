<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}

/* <div class="message success" onclick="this.classList.add('hidden')"><?= $message ?></div>*/
?>
<script type="text/javascript">
	 $(function() {
        toastr.options.positionClass = "toast-bottom-right";
		toastr.success('<?= $message ?>');
	});
</script>
<!--div class='alert alert-success' role='alert'> <? ?> </div>
