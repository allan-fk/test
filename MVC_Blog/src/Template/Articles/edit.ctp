<!-- File: src/Template/Articles/edit.ctp -->

<h1>Modifier un article</h1>
<?php
    echo $this->Form->create($article);
    echo $this->Form->control('title');
    echo $this->Form->control('body', ['rows' => '3']);
    // echo $this->Form->control('tags._title', ['options' => $tags]);
    echo $this->Form->control('tag_string', ['type' => 'text']);
    echo '<br>';
    echo $this->Form->button(__('Sauvegarder l\'article'));
    echo $this->Form->end();
?>