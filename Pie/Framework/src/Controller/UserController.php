<?php

namespace Controller;

use Core\Session;
use Model\UserModel;

class UserController extends \Core\Controller
{
    public function testAction($param = [])
    {
        $this->render("test.pie", ["test" => $param["id"]]);

        $user = new UserModel();
        $user->oneRelation("comment");
        $user->manyRelation("articles");
        var_dump($user->getRelations());
    }

    public function registerAction()
    {
        $this->render("register.pie");

        if($this->checkPost(["email", "password"])) {
            $user = new UserModel(["email" => $this->_http["post"]["email"], "password" => $this->_http["post"]["password"]]);
            if(!isset($user->id)) {
                $user->save();
                self::$_render = "Votre compte à été creer" . PHP_EOL;
            }
        }
    }

    public function loginAction()
    {
        $this->render("login.pie");

        $user = new UserModel(["email" => $this->_http["post"]["email"], "password" => $this->_http["post"]["password"]]);
        $result = $user->find();

        Session::fill($result["id"]);
        var_dump(Session::get());
    }
}