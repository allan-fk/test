<?php

namespace Core;

class Controller
{
    protected static $_render;
    protected $_http;


    public function __construct($http = [])
    {
        $request = new Request();
        $this->_http = $request->secureHttp($http);
    }

    public function __destruct()
    {
        echo self::$_render;
    }

    protected function checkPost($params = [])
    {
        $verif = array_map(function($item){
            if(!isset($_POST[$item])) {
                return false;
            }

            return true;
        }, $params);

        if(in_array(false, $verif)) {
            return false;
        }

        return true;
    }

    protected function render($view, $scope = [])
    {
        extract($scope);
        $file = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'src', 'View', str_replace('Controller', '', basename(get_class($this))), $view]);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if(!isset($title)) {
            $title = BASE_URI;
        }

        if (file_exists($file)) {

            ob_start();

            if($extension === "pie") {
                $template = new TemplateEngine($file);
                $template->parse();

                include "src/View/Template/template.php";
            } else {
                include($file);
            }

            $view = ob_get_clean();

            ob_start();
            include(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'src', 'View', 'index.php']));
            self::$_render = ob_get_clean();
        }
    }
}